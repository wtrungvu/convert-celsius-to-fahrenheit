import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = "Convert °C Celcius To °F Farenheit";

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: MyCustomForm(),
      ),
    );
  }
}

// Define a Custom Form Widget
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Define a corresponding State class. This class will hold the data related to our Form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a text controller. We will use it to retrieve the current value
  // of the TextField!

  // Use a TextEditingController
  /*
  A more powerful, but more elaborate approach, is to supply a TextEditingController as the controller property of the TextField or a TextFormField.
  To be notified when the text changes, we can listen to the controller using its addListener method.
   */
  var textEditingController_Celcius = new TextEditingController();
  var textEditingController_Farenheit = new TextEditingController();

  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a `GlobalKey<FormState>`, not a GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();
  String _celcius, _farenheit;
  bool _flag; // True -> calculate Celcius , False -> calculate Farenheit

  void classificationAndCalculation(bool flag, String value) {
    // flag = True -> canculate F
    if (flag == true) {
      _celcius = value;
      double numF = (double.parse(_celcius) * 1.8) + 32;
      // °F  =  ( °C × 1.8 ) +  32
      print("Farenheit: $numF");
      _farenheit = numF.toString();

      setState(() {
        textEditingController_Farenheit.text = _farenheit;
      });
    } else {
      // flag = Fales -> canculate C
      _farenheit = value;
      // °F  =  ( °C × 1.8 ) +  32
      double numC = (double.parse(_farenheit) * 1.8) + 32;
      print("Celcius: $numC");
      _celcius = numC.toString();

      setState(() {
        textEditingController_Celcius.text = _celcius;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey we created above
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(60, 0, 60, 0),
            child: TextField(
              controller: textEditingController_Celcius,
              onChanged: (String value) {
                _flag = true;
                classificationAndCalculation(_flag, value);
              },
              keyboardType: TextInputType.number,
              decoration:
                  InputDecoration(labelText: "Enter Your Number °C Celcius"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(60, 50, 60, 0),
            child: TextField(
              controller: textEditingController_Farenheit,
              onChanged: (String value) {
                _flag = false;
                classificationAndCalculation(_flag, value);
              },
              keyboardType: TextInputType.number,
              decoration:
                  InputDecoration(labelText: "Enter Your Number °F Farenheit"),
            ),
          ),
        ],
      ),
    );
  }
}
